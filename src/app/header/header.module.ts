import { HeaderComponent } from './header.component';
import { SettingsComponent } from './settings.component';
import { RunnerModule } from '../runner/runner.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
  declarations: [HeaderComponent, SettingsComponent],
  imports: [
    CommonModule,
    RunnerModule,
    MatDialogModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatSlideToggleModule,
    MatRadioModule,
  ],
  entryComponents: [SettingsComponent],
  exports: [HeaderComponent],
})
export class HeaderModule {}
