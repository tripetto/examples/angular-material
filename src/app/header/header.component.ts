import { Component, Input, ChangeDetectorRef, NgZone } from '@angular/core';
import { SettingsComponent } from './settings.component';
import { TripettoBuilderComponent } from '@tripetto/builder/angular';
import { RunnerComponent } from '../runner/runner.component';
import { AppComponent } from '../app.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'tripetto-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Input() app!: AppComponent;
  @Input() builder!: TripettoBuilderComponent;
  @Input() runner!: RunnerComponent;
  isSettingsOpened = false;

  constructor(private changeDetector: ChangeDetectorRef, private dialog: MatDialog, private ngZone: NgZone) {}

  /** Opens the settings dialog. */
  openSettings(): void {
    this.ngZone.run(() => {
      this.isSettingsOpened = true;

      this.dialog
        .open(SettingsComponent, {
          width: '90%',
          maxWidth: '500px',
          data: {
            runner: this.runner,
          },
        })
        .afterClosed()
        .subscribe(() => {
          this.isSettingsOpened = false;

          this.changed();
        });
    });
  }

  /**
   * We need this function since this component is a sibling of the runner component.
   * When the runner component detects a change, its siblings are not changed.
   * So we invoke this function so the header can detect the change.
   */
  changed() {
    this.changeDetector.detectChanges();
  }
}
