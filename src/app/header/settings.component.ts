import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RunnerComponent } from '../runner/runner.component';

@Component({
  selector: 'tripetto-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: {
      runner: RunnerComponent;
    }
  ) {}
}
