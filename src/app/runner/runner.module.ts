import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { assert } from '@tripetto/runner';
import { BlockComponent } from './helpers/blocks/block.component';
import { ButtonsComponent } from './helpers/buttons/buttons.component';
import { RunnerComponent } from './runner.component';
import { MarkdownDirective } from './helpers/markdown/markdown.directive';
import { PagesComponent } from './helpers/pages/pages.component';
import { ProgressbarComponent } from './helpers/progressbar/progressbar.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';

/** Import headless blocks */
import '@tripetto/block-calculator/runner';
import '@tripetto/block-device/runner';
import '@tripetto/block-evaluate/runner';
import '@tripetto/block-hidden-field/runner';
import '@tripetto/block-regex/runner';
import '@tripetto/block-setter/runner';
import '@tripetto/block-variable/runner';

/** Import runner blocks */
import { CheckboxBlockComponent, CheckboxBlock } from './blocks/checkbox/checkbox';
import { CheckboxesBlockComponent, CheckboxesBlock } from './blocks/checkboxes/checkboxes';
import { DropdownBlockComponent, DropdownBlock } from './blocks/dropdown/dropdown';
import { EmailBlockComponent, EmailBlock } from './blocks/email/email';
import { ExampleBlockComponent, ExampleBlock } from './blocks/example/example';
import { NumberBlockComponent, NumberBlock } from './blocks/number/number';
import { PasswordBlockComponent, PasswordBlock } from './blocks/password/password';
import { RadiobuttonsBlockComponent, RadiobuttonsBlock } from './blocks/radiobuttons/radiobuttons';
import { RatingBlockComponent, RatingBlock } from './blocks/rating/rating';
import { TextareaBlockComponent, TextareaBlock } from './blocks/textarea/textarea';
import { TextBlockComponent, TextBlock } from './blocks/text/text';
import { URLBlockComponent, URLBlock } from './blocks/url/url';

// We need to reference the blocks so we don't lose them in an optimized production build
assert({
  CheckboxBlock,
  CheckboxesBlock,
  DropdownBlock,
  EmailBlock,
  ExampleBlock,
  NumberBlock,
  PasswordBlock,
  RadiobuttonsBlock,
  RatingBlock,
  TextareaBlock,
  TextBlock,
  URLBlock,
});

@NgModule({
  declarations: [
    BlockComponent,
    ButtonsComponent,
    RunnerComponent,
    MarkdownDirective,
    PagesComponent,
    ProgressbarComponent,
    CheckboxBlockComponent,
    CheckboxesBlockComponent,
    DropdownBlockComponent,
    EmailBlockComponent,
    ExampleBlockComponent,
    NumberBlockComponent,
    PasswordBlockComponent,
    RadiobuttonsBlockComponent,
    RatingBlockComponent,
    TextBlockComponent,
    TextareaBlockComponent,
    URLBlockComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatProgressBarModule,
    MatRadioModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatToolbarModule,
  ],
  exports: [RunnerComponent],

  /** Block components are dynamically loaded, so register them here. */
  entryComponents: [
    CheckboxBlockComponent,
    CheckboxesBlockComponent,
    DropdownBlockComponent,
    EmailBlockComponent,
    ExampleBlockComponent,
    NumberBlockComponent,
    PasswordBlockComponent,
    RadiobuttonsBlockComponent,
    TextBlockComponent,
    TextareaBlockComponent,
    URLBlockComponent,
  ],
})
export class RunnerModule {}
