import { tripetto } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Dropdown } from '@tripetto/block-dropdown/runner';
import { Component } from '@angular/core';
import { MatSelect } from '@angular/material/select';

@Component({
  templateUrl: './dropdown.html',
})
export class DropdownBlockComponent extends BlockComponentFactory<DropdownBlock> {}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-dropdown',
  ref: DropdownBlockComponent,
})
export class DropdownBlock extends Dropdown {}
