import { tripetto } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Textarea } from '@tripetto/block-textarea/runner';
import { Component, NgZone, ViewChild } from '@angular/core';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs/operators';

@Component({
  templateUrl: './textarea.html',
})
export class TextareaBlockComponent extends BlockComponentFactory<TextareaBlock> {
  constructor(private _ngZone: NgZone) {
    super();
  }

  @ViewChild('autosize') autosize!: CdkTextareaAutosize;

  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this._ngZone.onStable.pipe(take(1)).subscribe(() => this.autosize.resizeToFitContent(true));
  }
}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-textarea',
  ref: TextareaBlockComponent,
})
export class TextareaBlock extends Textarea {
  onBlur(el: HTMLTextAreaElement): void {
    el.value = this.textareaSlot.string;
  }
}
