import { tripetto, castToString } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Number } from '@tripetto/block-number/runner';
import { Component } from '@angular/core';

@Component({
  templateUrl: './number.html',
})
export class NumberBlockComponent extends BlockComponentFactory<NumberBlock> {}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-number',
  ref: NumberBlockComponent,
})
export class NumberBlock extends Number {
  onFocus(el: HTMLInputElement) {
    /** In Firefox we lose focus when switching input type. */
    requestAnimationFrame(() => {
      el.focus();
    });

    // Switch to number type when focus is gained.
    el.value = castToString(this.numberSlot.value);
    el.type = 'number';
  }

  onBlur(el: HTMLInputElement) {
    // Switch to text type to allow number prefix and suffix.
    el.type = 'text';
    el.value = this.numberSlot.string;
  }
}
