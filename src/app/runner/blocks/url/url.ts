import { tripetto } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { URL } from '@tripetto/block-url/runner';
import { Component } from '@angular/core';

@Component({
  templateUrl: './url.html',
})
export class URLBlockComponent extends BlockComponentFactory<URLBlock> {}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-url',
  ref: URLBlockComponent,
})
export class URLBlock extends URL {
  onBlur(el: HTMLInputElement): void {
    el.value = this.urlSlot.string;
  }
}
