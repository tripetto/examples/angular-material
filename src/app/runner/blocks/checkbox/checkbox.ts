import { tripetto } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Checkbox } from '@tripetto/block-checkbox/runner';
import { Component } from '@angular/core';

@Component({
  templateUrl: './checkbox.html',
})
export class CheckboxBlockComponent extends BlockComponentFactory<CheckboxBlock> {}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-checkbox',
  ref: CheckboxBlockComponent,
})
export class CheckboxBlock extends Checkbox {}
