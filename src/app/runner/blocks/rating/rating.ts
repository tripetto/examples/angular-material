import { tripetto, Num, markdownifyToURL } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Rating } from '@tripetto/block-rating/runner';
import { Component } from '@angular/core';

@Component({
  templateUrl: './rating.html',
  styleUrls: ['./rating.scss'],
})
export class RatingBlockComponent extends BlockComponentFactory<RatingBlock> {
  get icon(): string {
    switch (this.block.props.shape) {
      case 'hearts':
        return 'favorite';
      case 'persons':
        return 'person';
      case 'stars':
        return 'star';
      case 'thumbs-up':
        return 'thumb_up';
      case 'thumbs-down':
        return 'thumb_down';
    }

    return this.block.props.shape || 'star';
  }

  get image(): string {
    return (
      (this.block.props.imageURL &&
        markdownifyToURL(this.block.props.imageURL, this.context, undefined, [
          'image/jpeg',
          'image/png',
          'image/svg',
          'image/gif',
        ])) ||
      ''
    );
  }

  get buttons() {
    const buttons: {
      readonly color: string;
      readonly className: string;
      readonly icon: string;
      readonly select: () => void;
    }[] = [];
    const icon = this.icon;

    for (let i = 1; i <= Num.max(1, this.block.steps); i++) {
      const checked = this.block.ratingSlot.value >= i;

      buttons.push({
        color: checked ? 'accent' : 'basic',
        className: checked ? '' : 'unchecked',
        icon,
        select: () => {
          this.block.ratingSlot.value = checked && this.block.ratingSlot.value === i ? i - 1 : i;

          if (!this.block.ratingSlot.value) {
            this.block.ratingSlot.clear();
          }
        },
      });
    }

    return buttons;
  }
}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-rating',
  ref: RatingBlockComponent,
})
export class RatingBlock extends Rating {}
