import { tripetto } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Password } from '@tripetto/block-password/runner';
import { Component } from '@angular/core';

@Component({
  templateUrl: './password.html',
})
export class PasswordBlockComponent extends BlockComponentFactory<PasswordBlock> {}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-password',
  ref: PasswordBlockComponent,
})
export class PasswordBlock extends Password {
  onBlur(el: HTMLInputElement): void {
    el.value = this.passwordSlot.string;
  }
}
