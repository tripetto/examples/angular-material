import { tripetto } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Text } from '@tripetto/block-text/runner';
import { Component } from '@angular/core';

@Component({
  templateUrl: './text.html',
})
export class TextBlockComponent extends BlockComponentFactory<TextBlock> {}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-text',
  ref: TextBlockComponent,
})
export class TextBlock extends Text {
  onBlur(el: HTMLInputElement): void {
    el.value = this.textSlot.string;
  }
}
