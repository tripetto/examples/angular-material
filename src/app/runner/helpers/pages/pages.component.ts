import { Component, Input } from '@angular/core';
import { IStoryline } from '@tripetto/runner';

@Component({
  selector: 'tripetto-runner-pages',
  templateUrl: './pages.component.html',
})
export class PagesComponent {
  @Input() storyline!: IStoryline;
}
