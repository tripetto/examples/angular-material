import { Component, Input } from '@angular/core';
import { IStoryline } from '@tripetto/runner';

@Component({
  selector: 'tripetto-runner-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss'],
})
export class ButtonsComponent {
  @Input() storyline!: IStoryline;
}
