import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { TripettoBuilderModule } from '@tripetto/builder/angular';
import { RunnerModule } from './runner/runner.module';
import { HeaderModule } from './header/header.module';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Import the builder blocks
import './builder/blocks';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    HeaderModule,
    TripettoBuilderModule,
    RunnerModule,
    BrowserAnimationsModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
